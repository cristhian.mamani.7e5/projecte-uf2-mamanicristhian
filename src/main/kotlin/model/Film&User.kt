package model

data class Film(var title: String, var director: String, val mainActor: String, var gender: String, var length: Int)

data class User(var user: String)